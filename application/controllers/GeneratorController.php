<?php
set_time_limit(300);
class GeneratorController extends Kaaz_Controller{
	
    public static $_menu_items = array('index'=>array('permission'=>11,'name'=>'Generate'));
	public function indexAction(){
	
		$x = new Kaaz_Tools_generator();
		
		/*
		 * FETCH ALL THE TABLES!
		 * So:
		$models = $x->getdb()->fetchAll("	SELECT TABLE_NAME
											FROM INFORMATION_SCHEMA.COLUMNS
											WHERE COLUMN_NAME = 'id'
											AND TABLE_SCHEMA = 'hearthbound'");
		 */
		
		
		$x->setModelName('UserSettings');
		$x->setTable('user_settings');
		
		$x->generate();
	}
}
