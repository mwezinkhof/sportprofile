<?php

class IndexController extends Kaaz_Controller{
	public function indexAction(){


	}
	
	public function loginAction(){

		$errors = array();
		$params = $this->getRequest()->getPost();
		if(count($params)>=2){
			$this->getAuthSession()->user = new Application_Model_User(array('name'=>$params['username']));
			if($this->getAuthSession()->user->id>0){
				if($this->getAuthSession()->user->authenticate($params['password'])){
					$this->_helper->redirector('index', 'index');
				}
				else{
					$errors[] = 'invalid_password';
				}
			}
			else{
				$errors[] = 'username_not_found';
			}
		}
		$this->view->errors = $errors;
    }
    public function logoutAction(){
	
		$session = $this->getAuthSession();
		$session->unsetAll();
		$this->_helper->redirector('login', 'Index');
    }
	public function registerAction(){
		$errors = array();
		if($this->getRequest()->isPost()){
			$r = $this->getRequest();
			$un = $r->getParam('username','');
			$e = $r->getParam('email','');
			$p = $r->getParam('password','');
			$pc = $r->getParam('password_confirmation','');
			$u = new Application_Model_User(array('name'=>$un));
			if($u->id>0){
				$errors[] = 'username_taken';
			}
			$u = new Application_Model_User(array('email'=>$e));
			if($u->id>0){
				$errors[] = 'email_taken';
			}
			if(strlen($p)<5){
				$errors[] = 'password_too_short';
			}
			else if($p != $pc){
				$errors[] = 'passwords_dont_match';
			}

			if(count($errors)===0){
				$u = new Application_Model_User();
				$u->name = $un;
				$u->password = $p;
				$u->email = $e;
				$u->save();
				$this->getAuthSession()->user = $u;
				$this->getAuthSession()->user->authenticate($p);
				$this->_helper->redirector('index', 'index');
			}
		}
		$this->view->errors = $errors;
	}
}
