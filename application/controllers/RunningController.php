<?php
class RunningController extends Kaaz_Controller{

	public function indexAction(){
		$uid = $this->getAuthSession()->user->id;
		$r = new Application_Model_Run();
		$this->view->lastRuns = $r->fetchAll(array('user_id = ?'=>$uid),array('rundate'=>'DESC'));
	}
	public function addAction(){
		$u = $this->getAuthSession()->user;
		$hours=0;
		$minutes=0;
		if($this->getRequest()->isPost() && $u->isAuthenticated()){
			$date = date('Y-m-d',strtotime($this->getRequest()->getParam('date',0)));//yyyy-mm-dd
			$distance = intval( 1000 * doubleval( $this->getRequest()->getParam('distance',0) ) ); //5.8
			sscanf($this->getRequest()->getParam('time',0), "%d:%d", $hours, $minutes);
			$time = $hours*60+$minutes;
			$cal = $this->getRequest()->getParam('calories',0);// 3

			//if everything is the same, its probably a double record :)
			$r = new Application_Model_Run(array(	'user_id'=>$u->id,
													'rundate'=>$date,
													'time'=>$time,
													'distance'=>$distance,
													'fat_burned'=>$cal ));
			$r->user_id = $u->id;
			$r->rundate = $date;
			$r->time = $time;
			$r->distance = $distance;
			$r->fat_burned = $cal;
			$r->save();
		}
	}
	public function getaverageAction(){
		$a = new Application_Model_Run();
		$p = $this->getRequest()->getParam('type','speed');
		$valid = array('speed','distance','duration');
		$criteria = array(	'speed'=> 'ROUND(distance/`time`*60/1000, 2)',
							'distance'=> 'ROUND(distance/1000, 2)',
							'duration'=> '`time`');
		if(in_array($p,$valid)){
			$selectedCriteria = $criteria[$p];

			$uid = $this->getAuthSession()->user->id;
			$r = $a->getdb()->query("	select `time`, distance, ".$selectedCriteria." as result, rundate
										from run
										where user_id = ".$uid."
										group by rundate
										order by rundate asc");
			$this->getHelper('json')->sendJson($r->fetchAll(),true);
		}
		die();
	}
	public function listAction(){
	}
	public function mylastrunAction(){
		if($this->getAuthSession()->user->isAuthenticated()){
			$r = new Application_Model_Run();
			$this->getHelper('json')->sendJson(
				$r->getdb()->query('select *
									from run
									where user_id = ' . $this->getAuthSession()->user->id . '
									order by rundate desc
									limit 0,1')->fetch()
				,true);
		}
		die();
	}
	public function myhistoryAction(){
		$rsp = array();
		if($this->getAuthSession()->user->isAuthenticated()){
			$r = new Application_Model_Run();
			$rsp = $r->getdb()->query('	SELECT rundate as `date`, round(distance/1000,2) as `distance (km)`, `time` as `time (min)`,ROUND(distance/`time`*60/1000,2) as `Average speed (km/h)`,fat_burned as calories
										from run
										where user_id = ' . $this->getAuthSession()->user->id .
										' order by rundate desc')->fetchAll();
		}
		$this->getHelper('json')->sendJson($rsp,true);
	}
	public function statsAction(){

	}
	public function scheduleAction(){

	}
	
}