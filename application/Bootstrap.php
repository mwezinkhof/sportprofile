<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap{

	public function _initRoutes(){
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController->getRouter();
	}

	protected function _initConfig(){
		$config = new Zend_Config($this->getOptions());
		Zend_Registry::set('config', $config);
	}
	protected function _initDb(){
		$resource = $this->getPluginResource('multidb');
		$resource->init();
		try{
			Zend_Registry::set('dbAdapter', $resource->getDb('db1'));
			Zend_Registry::get('dbAdapter')->setFetchMode(Zend_Db::FETCH_ASSOC);
			Zend_Registry::get('dbAdapter')->query("SET NAMES 'utf8'");
			Zend_Registry::get('dbAdapter')->query("SET CHARACTER SET 'utf8'");
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

}

