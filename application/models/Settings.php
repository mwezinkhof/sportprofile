<?php
/**
 * Class object_Settings
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `settings`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:09
 */
class Application_Model_Settings extends Application_Model_Object_Settings {

	/**
	 * These Values are used in the table `settings`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'name', 'value_type', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}