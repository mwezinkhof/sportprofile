<?php
/**
 * Class object_Run
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `run`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 12:02
 */
class Application_Model_Run extends Application_Model_Object_Run {

	/**
	 * These Values are used in the table `run`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'user_id', 'rundate', 'distance', 'time', 'fat_burned', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}