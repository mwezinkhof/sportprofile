<?php
/**
 * Class object_Media
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `media`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:10
 */
class Application_Model_Media extends Application_Model_Object_Media {

	/**
	 * These Values are used in the table `media`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'name', 'episodes', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}