<?php

class Application_Model_Menu {
	protected $_items;
	
	public function __construct() {
		$this->_items = array();
		$this->addItem('Home', '/');

		$this->addItem('Cards','card','');
		$this->addItem('Decks','deck','');

		$this->addItem('Settings','Settings','');

	}
	
	function addItem($name,$url,$node='',$childs=array()){
		
		$this->_items[] = array('name'=>$name,'url'=>$url,'permission'=>$node,'childs'=>$childs);
	}
	protected function _addItem($name,$url,$node=''){
		
		return array('name'=>$name,'url'=>$url,'permission'=>$node,'childs'=>array());
	}
	function getMenuItems(){
		return $this->_items;
	}
	
}