<?php
/**
 * Class object_UserSettings
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `user_settings`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:12
 */
class Application_Model_UserSettings extends Application_Model_Object_UserSettings {

	/**
	 * These Values are used in the table `user_settings`
	 * 
	 * @var Array
	 */
	protected $_valid_properties = array('id', 'setting_id', 'value', 'user_id', );

	public function __constructor($id=0, $column='id') {
		parent::__construct($id,$column);
		//Code;
	}

	public function save($saveRelations=false) {
		parent::save();
		//custom save;
	}

}