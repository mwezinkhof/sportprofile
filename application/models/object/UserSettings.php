<?php
/**
 * Class object_UserSettings
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `user_settings`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:12
 */
abstract class Application_Model_Object_UserSettings extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'user_settings';

	/**
	 * @var int(11)
	 */
	protected $_setting_id;

	/**
	 * @var varchar(50)
	 */
	protected $_value;

	/**
	 * @var int(11)
	 */
	protected $_user_id;

	protected function _set_setting_id($val) {
		$this->_setting_id = (int) $val;
	}

	protected function _set_value($val) {
		$this->_value = substr($val,0, 50);
	}

	protected function _set_user_id($val) {
		$this->_user_id = (int) $val;
	}

}