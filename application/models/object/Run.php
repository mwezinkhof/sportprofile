<?php
/**
 * Class object_Run
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `run`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 12:02
 */
abstract class Application_Model_Object_Run extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'run';

	/**
	 * @var int(11)
	 */
	protected $_user_id;

	/**
	 * @var date
	 */
	protected $_rundate;

	/**
	 * @var int(11)
	 */
	protected $_distance;

	/**
	 * @var int(11)
	 */
	protected $_time;

	/**
	 * @var int(11)
	 */
	protected $_fat_burned;

	protected function _set_user_id($val) {
		$this->_user_id = (int) $val;
	}

	protected function _set_rundate($val) {
		$this->_rundate = $val;
	}

	protected function _set_distance($val) {
		$this->_distance = (int) $val;
	}

	protected function _set_time($val) {
		$this->_time = (int) $val;
	}

	protected function _set_fat_burned($val) {
		$this->_fat_burned = (int) $val;
	}

}