<?php
/**
 * Class object_User
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `user`.
 * Validation rules should be modified in the database.
 * Generated on 2014-06-07 @ 12:16
 */
abstract class Application_Model_Object_User extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'user';

	/**
	 * @var varchar(40)
	 */
	protected $_name;

	/**
	 * @var varchar(150)
	 */
	protected $_email;

	/**
	 * @var varchar(128)
	 */
	protected $_password;

	/**
	 * @var varchar(128)
	 */
	protected $_salt;

	/**
	 * @var int(11)
	 */
	protected $_group_id;

	protected function _set_name($val) {
		$this->_name = substr($val,0, 40);
	}

	protected function _set_email($val) {
		$this->_email = substr($val,0, 150);
	}

	protected function _set_password($val) {
		$this->_password = substr($val,0, 128);
	}

	protected function _set_salt($val) {
		$this->_salt = substr($val,0, 128);
	}

	protected function _set_group_id($val) {
		$this->_group_id = (int) $val;
	}

}