<?php
/**
 * Class object_Settings
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `settings`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:09
 */
abstract class Application_Model_Object_Settings extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'settings';

	/**
	 * @var varchar(50)
	 */
	protected $_name;

	/**
	 * @var varchar(50)
	 */
	protected $_value_type;

	protected function _set_name($val) {
		$this->_name = substr($val,0, 50);
	}

	protected function _set_value_type($val) {
		$this->_value_type = substr($val,0, 50);
	}

}