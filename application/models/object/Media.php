<?php
/**
 * Class object_Media
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `media`.
 * Validation rules should be modified in the database.
 * Generated on 2014-07-01 @ 13:10
 */
abstract class Application_Model_Object_Media extends Kaaz_Model {

	/**
	 * The database table used
	 * 
	 * @var String
	 */
	protected $_tablename = 'media';

	/**
	 * @var varchar(100)
	 */
	protected $_name;

	/**
	 * @var int(11)
	 */
	protected $_episodes;

	protected function _set_name($val) {
		$this->_name = substr($val,0, 100);
	}

	protected function _set_episodes($val) {
		$this->_episodes = (int) $val;
	}

}