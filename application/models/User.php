<?php

/**
 * Class object_User
 * @Author Class: `Kaaz_Tools_generator` Version: 0.1
 * 
 * This is a generated class
 * 
 * Generation Details:
 * Based on the database table `user`.
 * Validation rules should be modified in the database.
 * Generated on 2012-07-08 @ 13:20
 */
class Application_Model_User extends Application_Model_Object_User {

	/**
	 * These Values are used in the table `user`
	 * 
	 * @var Array
	 */
	protected $_user_group;
	protected $_valid_properties = array('id', 'name', 'email', 'group_id', 'password','salt');
	protected $_authenticated = false;
	protected $_modules = false;

	protected function _generateSalt() {
		$this->_salt = hash('sha256', mt_rand() . microtime(true));
	}

	protected function _generateSaltedPassword($password) {

		if ($this->_salt == '') {
			$this->_generateSalt();
		}
		$this->_password = hash('sha256', $password . $this->salt);
	}

	protected function _set_password($password) {
		if ($this->_can_dirty) {
			$this->_generateSaltedPassword($password);
		} else {
			$this->_password = $password;
		}
	}

	public function getPermissionLevel() {
		if ($this->_user_group instanceof Application_Model_UserGroup) {
			return $this->_user_group->permission_level;
		}
		return 0;
	}

	public function authenticate($password) {

		if ($this->_id > 0) {
			if ($this->_password == hash('sha256', $password . $this->_salt)) {
				$this->_authenticated = true;
				return true;
			}
		}
		return false;
	}

	public function isAuthenticated() {
		return $this->_authenticated;
	}

}