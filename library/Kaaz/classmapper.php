<?php
/**
 * Class baseclassmapper
 *
 * @category Model
 * @version 0.1
 *
 * This baseclass provides a simple form of reflection. It currently provides the ability to find classes based on an interface.
 */
class Kaaz_classmapper{

	/**
	 * The name of the interface to match.
	 * @var String
	 */
	protected $_classinterface;

	/**
	 * The base path of the classes.
	 * @var String
	 */
	protected $_classBasePath;//

	/**
	 * The location where all classes are forced to load.
	 * @var String
	 */
	protected $_classlocation;

	/**
	 * An array of strings containing names of classes which to ignore.
	 * @var array<string>
	 */
	protected $_classesToIgnore = array();

	/**
	 * Constructor of the class
	 */
	public function __construct() {
		$this->_classBasePath = dirname(__FILE__).'/../../Application/';
	}

	/**
	 * It looks up through all classes, and returns all classes implementing the interface set in $this->_classinterface;<br/>
	 * It ignores all classes set in $this->_classesToIgnore;<br/>
	 * Optional: Set a location to load to force classes to be loaded. This location can be set in $this->_classlocation;
	 * @return Array With all the matching classes
	 */
	public function findClasses(){

		$ret = array();
		$lookinfor = array();
		if(!empty($this->_classlocation)){
			$dh = opendir($this->_classBasePath . $this->_classlocation);
			if ($dh) {
				while (($file = readdir($dh)) !== false) {
				    
					if(substr($file, -4) == '.php'){
					    $lookinfor[] = substr($file,0,-4);
					    if(!class_exists(substr($file,0,-4))){
						include $this->_classBasePath . $this->_classlocation . $file;
					    }
					}
				}
				closedir($dh);
				$dh=null;
			}
		}

		foreach($lookinfor as $className) {
			if( !in_array($className, $this->_classesToIgnore) && in_array($this->_classinterface, class_implements($className)) ) {
				$ret[] = $className;
			}
		}
		return $ret;
	}
}

