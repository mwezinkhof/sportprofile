<?php
/**
 * Toolset for Controllers
 */
abstract class Kaaz_Controller extends Zend_Controller_Action{

	protected $_model=false;
	protected $_modelObj =null;
	protected $_jsonrequest = false;
	protected $_mask = array();
	protected $_orderBy = array('id'=>'ASC');
	protected $_request;
	protected $_auth_session;
	protected $_soap_client;
	protected $_can_soap = true;

	protected function getAuthSession(){
	    if(!$this->_auth_session instanceof Zend_Session_Namespace){
			$this->_auth_session = new Zend_Session_Namespace('sportprofile_user');
	    }
		if(!isset($this->_auth_session->user) || !($this->_auth_session->user instanceof Application_Model_User) ){
			$this->_auth_session->user = new Application_Model_Run();
		}
	    return $this->_auth_session;
	}
	public function IsUserLoggedIn(){
		if(isset($this->getAuthSession()->user) && $this->getAuthSession()->user instanceof Application_Model_User){
			return $this->getAuthSession()->user->isAuthenticated();
		}
		return false;
	}
	public function setUserVariables(){
	   
	    $name = '';
	    $loggedIn = false;
	    $session = $this->getAuthSession();
	    if(isset($session->user) && $session->user instanceof Application_Model_User){
			$name = $session->user->name;
			$loggedIn = $session->user->isAuthenticated();
	    }
	    $this->view->user = array('name'=>$name,'authenticated'=>$loggedIn,'permission_nodes'=>array());
	    $this->view->controllerName = $this->_request->getControllerName();
		$this->view->viewName = $this->_request->getActionName();
	}
	public function init(){
	    
	    if($this->getRequest()->getParam('json')){
	        $this->_jsonrequest = true;
	    }
		$this->_auth_session = $this->getAuthSession();
	    $this->_request = $this->getRequest();
	    if(!isset($this->_auth_session->user)
		|| !$this->_auth_session->user instanceof Application_Model_User
		|| !$this->_auth_session->user->isAuthenticated()){
			if($this->_request->getControllerName() !== 'index'){
				$this->_helper->redirector('login', 'index');
			}
	    }
	    if(!$this->_jsonrequest){
			$this->setUserVariables();    
	    }
	}
	public function indexAction(){}

	public function addAction(){
		$this->view->model = $this->getModel();
		$this->_helper->viewRenderer('edit');
	}

	public function editAction(){
		$m = $this->getModel();
		if($m !== null){
			$m->loadFromTable($this->getRequest()->getParam('id'));
		}
		$this->view->model = $m;
	}
	public function listAction(){
		if($this->_jsonrequest && $this->getModel() !== null){

			$this->getHelper('json')->sendJson($this->getModel()->fetchAll($this->_getFilterSQL(),$this->_orderBy),true);
		}
	}
	public function deleteAction(){

		if($this->_jsonrequest){

			$response = array('success'=>false);
			$id = $this->_validateId($this->getRequest()->getParam('id'));
			$m = $this->getModel();
			$m->loadFromTable($id);
			if($m->id == $id){
				$response['success'] = $m->deleteMe();
			}
			$this->getHelper('json')->sendJson($response,true);

		}
	}
	public function getModel(){
		if($this->_model != false){
			if(!($this->_modelObj instanceof $this->_model)){
				$this->_modelObj = new $this->_model();
			}
		}
		return $this->_modelObj;
	}
	protected function _setModel($name){
		$this->_model = $name;
	}
	protected function _setFilterMask(array $mask){
		$this->_mask = $mask;
	}
	protected function _setOrderBy(array $ob){
		$this->_orderBy = $ob;
	}
	/**
	 * @name Validate Id
	 * @example $this->_validateId("derp") Returns 0;
	 * @param String/id $id
	 * @return Int >= 0
	 */
	protected function _validateId($id){
		return max(0,intval($id));
	}

	protected function _getFilterSQL(array $valid_masks=array()){

		$where = array();
		$filter = $this->_getFilter($valid_masks);
		foreach($filter as $mask){
			$where[$mask->property] = $mask->value;
		}
		return $where;
	}
	protected function _getFilter(array $valid_masks=array()){

		$filter = @json_decode($this->getRequest()->getParam('filter'));

		if($filter == null) $filter = array();
		if(count($valid_masks) == 0) $valid_masks = &$this->_mask;

		foreach($filter as $key => $mask){
			if(!in_array($mask->property,$valid_masks)){
				unset($filter[$key]);
			}
		}
		return $filter;
	}
	/**
	 * @deprecated Will be removed when filter is implemented!
	 * @param array $valid_masks
	 * @return type
	 */
	protected function _getSQLFilter(array $valid_masks=array()){
		$where = array();
		$filter = $this->_getFilter($valid_masks);
		foreach($filter as $mask){
			$where[$mask->property.' = ?'] = $mask->value;
		}
		return $where;
	}
}