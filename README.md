TEST USER(S)
=========
You can login in the site with: hodor / hodor123


DATABASE
========
1. Make a database called sportprofile using utf-8
2. select the database
3. copy the contents of /docs/sportprofile.sql
4. pasterino the contents into the sql input
5. ?????
6. profit

HTDOCS
==========
Make a folder in your htdocs called "sportprofile"

Git init in that directory

See /docs/git.txt for more more info




Setting Up Your VHOST
=====================

The following is a sample VHOST you might want to consider for your project.


    <VirtualHost *:80>
       DocumentRoot "C:/xampp/htdocs/sportprofile"
       ServerName .local
    
       # This should be omitted in the production environment
       SetEnv APPLICATION_ENV development
    
       <Directory "C:/xampp/htdocs/sportprofile">
           Options Indexes MultiViews FollowSymLinks
           AllowOverride All
           Order allow,deny
           Allow from all
       </Directory>
  
    </VirtualHost>
