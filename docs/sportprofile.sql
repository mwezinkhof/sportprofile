/*
MySQL Data Transfer
Source Host: localhost
Source Database: sportprofile
Target Host: localhost
Target Database: sportprofile
Date: 3-7-2014 21:12:25
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `episodes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for run
-- ----------------------------
DROP TABLE IF EXISTS `run`;
CREATE TABLE `run` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rundate` date NOT NULL,
  `distance` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `fat_burned` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for run_media
-- ----------------------------
DROP TABLE IF EXISTS `run_media`;
CREATE TABLE `run_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `episode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for user_settings
-- ----------------------------
DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) NOT NULL,
  `value` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `media` VALUES ('1', 'One Piece', '700');
INSERT INTO `run` VALUES ('5', '1', '2014-07-01', '6200', '48', '391');
INSERT INTO `run` VALUES ('6', '3', '2014-07-02', '5000', '0', '0');
INSERT INTO `run` VALUES ('7', '1', '2014-07-03', '6400', '46', '405');
INSERT INTO `settings` VALUES ('1', 'use_media', 'boolean');
INSERT INTO `user` VALUES ('1', 'Kaaz', 'maik_wezinkhof@hotmail.com', '53b59a1cf61d6b6b507f41d0581ecc3d02c53b6904205035e5c4c0556e0555d3', '0dd16dda3ed446155b16df4b32e4f67aa688c1405ebd7373b0e86e82f6e9e42d', '0');
INSERT INTO `user` VALUES ('2', 'maik', 'xlkaaz@gmail.com', '2f8832cf019121644ae75ab28db97257538a6fd075b83fb27f058fb5ca3bd8f3', '9a2e5986bdfb235197ded8cd84570a1d445ea8dabe09240ff19a269110b043cc', '0');
INSERT INTO `user` VALUES ('3', 'hodor', 'maik_wezinkhof1@hotmail.com', 'dd260e4b07dbce9cc8a0b5bfd2aea7593e075a195390f1e52cbee7f82a7b52ea', 'd839af93f7a97407aea895675dd960e0c35a4b5d8a90a1d162e18c8fac243e25', '0');
