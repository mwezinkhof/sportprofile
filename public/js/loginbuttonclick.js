$(document).ready(function() {
    $("#show_login").click(function() {
        $("#login_form_box").show(200);
        $("#login_form").show(100);
        $("#hide_login").show();                
        $("#show_login").hide();        
    });

    $("#hide_login").click(function(){
        $("#login_form_box").hide(200);
        $("#hide_login").hide();
        $("#login_form").hide(100);
        $("#show_login").show();
    });

});