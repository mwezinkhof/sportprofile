$(document).ready(
    function () {
        $('.nav li').hover(
            function () {
                $('a', this).first().addClass('activeMenuItem');
                $('ul', this).stop().fadeIn();
            },
            function () {
                $('a', this).first().removeClass('activeMenuItem');
                $('ul', this).stop().fadeOut();
            }
        );
    }
);