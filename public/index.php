<?php
// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(__DIR__ . '/../application'));
defined('PUBLIC_PATH')
|| define('PUBLIC_PATH', realpath(__DIR__ . '/../public'));
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));
require_once 'Zend/Loader/Autoloader.php';
$autoLoader = Zend_Loader_Autoloader::getInstance();
$autoLoader->registerNamespace('Kaaz_');

/** Zend_Application */
require_once 'Zend/Application.php';
$dh = opendir(APPLICATION_PATH . '/models/object/');
if ($dh) {
	while (($file = readdir($dh)) !== false) {
		if($file != "." && $file != ".."){
			require_once APPLICATION_PATH . '/models/object/' . $file;
		}
	}
	closedir($dh);
	$dh=null;
}
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
try{
	if(!defined('CRON_MODE') || CRON_MODE === false){
		$application->bootstrap()->run();
	}
}
catch(Exception $e){
	var_dump($e);
}